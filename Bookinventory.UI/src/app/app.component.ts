import { Component } from '@angular/core';
import { BookInventory } from './models/book-inventory';
import { BookInventoryService } from './services/book-inventory.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'Bookinventory.UI';
  booked : BookInventory[] = [];
  bookToEdit?: BookInventory;



  constructor (private bookInventoryService: BookInventoryService){}

  ngOnInit() : void 
  {
    this.bookInventoryService.getbook().subscribe((result: BookInventory[]) => (this.booked=result));

  }

  updateBookList(booked: BookInventory[]){
    this.booked=booked;
  }

  initNewBook(){
    this.bookToEdit = new BookInventory();
  }

  editBook(book: BookInventory){
    this.bookToEdit= book;
  }
}
