import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { BookInventory } from '../models/book-inventory';

@Injectable({
  providedIn: 'root'
})
export class BookInventoryService {

 private  url="BookInventory";

  constructor(private http: HttpClient) { }


  public getbook() : Observable<BookInventory[]>{
     return this.http.get<BookInventory[]>(`${environment.apiUrl}/${this.url}`) ;
  }

  public updateBook(book: BookInventory) : Observable<BookInventory[]>{
    return this.http.put<BookInventory[]>(`${environment.apiUrl}/${this.url}`, book) ;
 }

 public createBook(book: BookInventory) : Observable<BookInventory[]>{
  return this.http.post<BookInventory[]>(`${environment.apiUrl}/${this.url}`, book) ;
}


public deleteBook(book: BookInventory) : Observable<BookInventory[]>{
  return this.http.delete<BookInventory[]>(`${environment.apiUrl}/${this.url}/${book.id}`) ;
}
}
