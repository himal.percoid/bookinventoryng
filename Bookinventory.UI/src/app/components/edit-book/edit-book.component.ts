import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BookInventory } from 'src/app/models/book-inventory';
import { BookInventoryService } from 'src/app/services/book-inventory.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {


  @Input() book?:BookInventory;

  @Output() booksUpdated =new EventEmitter<BookInventory[]>();

  constructor(private BookInventoryService: BookInventoryService) { }

  ngOnInit(): void {
  }

  updateBook(book:BookInventory){
    this.BookInventoryService.updateBook(book).subscribe((booked: BookInventory[]) => this.booksUpdated.emit(booked));
  }

  deleteBook(book:BookInventory){
    this.BookInventoryService.deleteBook(book).subscribe((booked: BookInventory[]) => this.booksUpdated.emit(booked));
  }

  createBook(book:BookInventory){
    this.BookInventoryService.createBook(book).subscribe((booked: BookInventory[]) => this.booksUpdated.emit(booked));
  }


}
