﻿using BookInventoryAPI.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookInventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookInventoryController : ControllerBase
    {
        private readonly DataContext _context;

        public BookInventoryController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]

        public async Task<ActionResult<List<BookInventory>>> GetBooks()
        {
            return Ok(await _context.BookInventories.ToListAsync());
        }

        [HttpPost]

        //create method

        public async Task<ActionResult<BookInventory>> CreateBook(BookInventory book)
        {
            _context.BookInventories.Add(book);
            await _context.SaveChangesAsync();

            return Ok(await _context.BookInventories.ToListAsync());
        }

        //update method


        [HttpPut]

        public async Task<ActionResult<BookInventory>> UpdateBook(BookInventory book)
        {
            var dbBook =await _context.BookInventories.FindAsync(book.Id);
            if (dbBook == null)
                return BadRequest("Book not Found..");

            dbBook.Id = book.Id;
            dbBook.Name = book.Name;
            dbBook.Author = book.Author;
            dbBook.Genre = book.Genre;
            dbBook.Publisher = book.Publisher;
            dbBook.Language = book.Language;
            dbBook.Price = book.Price;

            await _context.SaveChangesAsync();

            return Ok(await _context.BookInventories.ToListAsync());


        }

        //Delete method

        [HttpDelete("{id}")]
        public async Task<ActionResult<BookInventory>> DeleteBook(int id)
        {
            var dbBook = await _context.BookInventories.FindAsync(id);
            if (dbBook == null)
            return BadRequest("Book not Found..");

            _context.BookInventories.Remove(dbBook);
            await _context.SaveChangesAsync();

            return Ok(await _context.BookInventories.ToListAsync());
            

        }

        
    }
}
