﻿using Microsoft.EntityFrameworkCore;


namespace BookInventoryAPI.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}
        public DbSet<BookInventory> BookInventories => Set<BookInventory>();    
    }
}
